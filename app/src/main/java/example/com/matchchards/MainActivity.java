package example.com.matchchards;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.new_game)
    Button mNewGame;

    @BindView(R.id.resume_game)
    Button mResumeGame;

    @BindView(R.id.highscores)
    Button mHighscores;

    @BindView(R.id.settings)
    Button mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this , NewGameActivity.class);
                startActivity(intent);

            }
        });


    }
}
